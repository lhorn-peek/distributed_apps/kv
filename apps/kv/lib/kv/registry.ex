defmodule KV.Registry do
    use GenServer

    # start genServer 'Registry', @params(server_callbacks_module, init_args, genserver_config)
    #
    def start_link(name) do
        GenServer.start_link(__MODULE__, name, name: name)
    end

    def stop(server) do
        GenServer.stop(server)
    end

    # lookup bucket on server (ie. bucket pid by name) -call
    #
    def lookup(server, name) when is_atom(server) do
        case :ets.lookup(server, name) do
            [{^name, pid}] -> {:ok, pid}
            [] -> :error
        end
    end

    @doc """
    create bucket (store bucket pid on server) -cast
    """
    def create(server, name) do
        GenServer.call(server, {:create, name})
    end



    # SERVER CALLBACKS

    # init server state(ie. names_state) with empty map %{}
    #
    def init(table) do
        names = :ets.new(table, [:named_table, read_concurrency: true])
        refs = %{}
        {:ok, {names, refs}}
    end


    # handle lookup request, @params(request, caller, serverState)
    # 1)
    # def handle_call({:lookup, name}, _from, {names, _} = state) do
    #     {:reply, Map.fetch(names, name), state}
    # end

    # handle create request(ie. create bucket, store pid by name), @params(request, serverState)
    # 1)
    # def handle_cast({:create, name}, {names, refs}) do
    #     case Map.has_key?(names, name) do
    #         false ->
    #             {:ok, pid} = KV.Bucket.Supervisor.start_bucket
    #             ref = Process.monitor(pid)
    #             refs = Map.put(refs, ref, name)
    #             names = Map.put(names, name, pid)
    #             {:noreply, {names, refs}}
    #         _____ ->
    #             {:noreply, {names, refs}}
    #     end
    # end

    # handle create request(ie. create bucket, store pid by name), @params(request, serverState)
    # 2)
    def handle_call({:create, name}, _from, {names, refs}) do
        # 5. Read and write to the ETS table instead of the map
        case lookup(names, name) do
            {:ok, pid} ->
                {:reply, pid, {names, refs}}
            :error ->
                {:ok, pid} = KV.Bucket.Supervisor.start_bucket
                ref = Process.monitor(pid)
                refs = Map.put(refs, ref, name)
                :ets.insert(names, {name, pid})
                {:reply, pid, {names, refs}}
        end
    end



    def handle_info({:DOWN, ref, :process, _pid, _reason}, {names, refs}) do
        {name, refs} = Map.pop(refs, ref)
        :ets.delete(names, name)
        {:noreply, {names, refs}}
    end

    def handle_info(_msg, state) do
        {:noreply, state}
    end
end