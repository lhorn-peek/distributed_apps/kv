defmodule KV.BucketTest do
    use ExUnit.Case, async: true

    setup do
        {:ok, bucket} = KV.Bucket.start_link
        {:ok, bucket: bucket}
    end

    test "stores values by key", %{bucket: buckett} do
        assert KV.Bucket.get(buckett, "milk") == nil

        KV.Bucket.put(buckett, "milk", 3)
        assert KV.Bucket.get(buckett, "milk") == 3
    end

    test "deletes value by key", %{bucket: buckett} do
        KV.Bucket.put(buckett, "milk", 3)

        assert KV.Bucket.delete(buckett, "milk") == 3
        assert KV.Bucket.get(buckett, "milk") == nil
    end
end