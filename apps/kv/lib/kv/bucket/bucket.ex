defmodule KV.Bucket do
    @doc """
    Starts a new bucket
    """
    def start_link do
        Agent.start_link(fn -> %{} end)
    end

    @doc """
    Gets a value from the bucket by key
    """
    def get(bucket, key) do
        Agent.get(bucket, &Map.get(&1, key))
    end

    @doc """
    puts a value for given key into bucket
    """
    def put(bucket, key, value) do
        Agent.update(bucket, &Map.put(&1, key, value))
    end

    @doc """
    delete key/value from bucket
    """
    def delete(bucket, key) do
        Agent.get_and_update(bucket, &Map.pop(&1, key))
    end
end